﻿#include "sprite.h"

int Sprite::getX()
{
	return x;
}

int Sprite::getY()
{
	return y;
}

void Sprite::setX(int new_x)
{
	x = new_x;
}

void Sprite::setY(int new_y)
{
	y = new_y;
}