#include <cstring>

#include "hs_table.h"

using namespace std;

int HS_table::getScore()
{
	return score;
}

char *HS_table::getName()
{
	return name;
}

void HS_table::setName(char *n)
{
	strcpy(name, n);
}

HS_table& HS_table::operator<<(int s)
{
	score = s;
	return *this;
}

HS_table& HS_table::operator=(HS_table& src)
{
	strcpy(name, src.getName());
	score = src.getScore();
	return *this;
}