﻿#include <cstring>

#include "potter.h"
#include "gengine.h"
#include "vengine.h"

using namespace std;

extern GEngine ge;
extern VEngine ve;

Potter::Potter()
{
	gems = 0;
	scroll_placed = false;
}

void Potter::reset()
{
	gems = 0;
	scroll_placed = false;
}

int Potter::move(int key)
{
	int newX = x, newY = y;
	int rnd_x, rnd_y;
	char gem_count[] = "xx/1x";
	
	switch (key)
	{
		case KEY_LEFT:
			newX--;
			break;
		case KEY_RIGHT:
			newX++;
			break;
		case KEY_UP:
			newY--;
			break;
		case KEY_DOWN:
			newY++;
			break;
	}

	if (ge.isPathable(newX, newY))
	{
		if (ge.getType(newX, newY) == GEM)
		{
			gems++;
			strcpy(gem_count, ge.int_to_str(gems));
			strcat(gem_count, "/");
			strcat(gem_count, ge.int_to_str(9+ge.getCurr_level()));
			ge.addScore(10);
			wattron(ve.wd, COLOR_PAIR(2));
			ve.center(ge.int_to_str(ge.getScore()), 4, ve.wd, 3);
			ve.center(gem_count, 5, ve.wd, 3);
			wborder(ve.wd, 0, 0, 0, 0, 0, 0, 0, 0);
			wattroff(ve.wd, COLOR_PAIR(2));
			wrefresh(ve.wd);
		}

		if (gems == 9 + ge.getCurr_level())
		{
			if (! scroll_placed)
			{
				do
				{
					rnd_x = ge.get_random_loc(ge.getMaxX()-1);
					rnd_y = ge.get_random_loc(ge.getMaxY()-1);
				} while (! ge.getType(rnd_x, rnd_y) == PATH);
				ge.pinpoint(rnd_x, rnd_y, SCROLL);
				ve.pin(rnd_x, rnd_y, SCROLL);
				scroll_placed = true;
			}
			
			if (ge.getType(newX, newY) == SCROLL)
			{
				ge.addScore(100);
				return 1;
			}
		}
		
		ge.pinpoint(x, y, PATH);
		ve.pin(x, y, PATH);
		x = newX;
		y = newY;
		ge.pinpoint(x, y, POTTER);
		ve.pin(x, y, POTTER);
	}
	else
		if (ge.getType(newX, newY) == TRAAL || ge.getType(newX, newY) == GNOME)
			return -1;
	return 0;
}