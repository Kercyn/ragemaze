﻿#include <cstring>
#include <sstream>
#include <cstdlib>
#include <ctime>
#include <iostream>

#include "gengine.h"
#include "vengine.h"
#include "hsengine.h"

extern VEngine ve;
extern HSEngine hse;
extern ISoundEngine *se;
extern int now_playing;

using namespace std;

GEngine::GEngine()
{
	curr_level = 0;
	max_level = 3;
	score = 0;
	name = new char[1];
	path = new char[strlen("levels/levelx") + 1];
	strcpy(path, "levels/levelx");
}

GEngine::~GEngine()
{
	delete[] name;
	delete[] path;
}

void GEngine::setCurr_level(int l)
{
	curr_level = l;
}

void GEngine::setName(char *n)
{
	strcpy(name, n);
}

void GEngine::addScore(int s)
{
	score += s;
}

int GEngine::getCurr_level()
{
	return curr_level;
}

int GEngine::getMax_level()
{
	return max_level;
}

char *GEngine::getName()
{
	return name;
}

int GEngine::getMaxX()
{
	return gmap.getMaxX();
}

int GEngine::getPlayerX()
{
	return player.getX();
}

int GEngine::getPlayerY()
{
	return player.getY();
}

int GEngine::getMaxY()
{
	return gmap.getMaxY();
}

int GEngine::getType(int x, int y)
{
	return gmap.getType(x,y);
}

int GEngine::getAux(int x, int y)
{
	  return gmap.getAux(x,y);
}

int GEngine::getScore()
{
	return score;
}

int GEngine::getTrSpeed()
{
	return tr_mob.getSpeed();
}

int GEngine::getGnSpeed()
{
	return gn_mob.getSpeed();
}

bool GEngine::isPathable(int whichX, int whichY)
{
	return gmap.isPathable(whichX, whichY);
}

void GEngine::new_level()
{
	char ch_l = ++curr_level + 48;
	player.reset();
	path[strlen(path)-1] = ch_l;
	path[strlen(path)] = '\0';
	gmap.load(path);
	tr_mob.accelerate();
	gn_mob.accelerate();
}

char *GEngine::int_to_str(int num)
{
	static char buf[6] = {0};
	int i = 6;
	
	for (; num && i; --i, num /= 10)
		buf[i]="0123456789"[num % 10];
	return &buf[i+1];
}

int GEngine::get_random_loc(int rng)
{
	srand((unsigned)time(0));
	return ((rand()%rng)+1);
}

char *GEngine::get_random_song()
{
	static char sound_path[] = "sounds/x.ogg";
	int song;
	
	srand((unsigned)time(0));
	do
	{
		song = rand()%3;
	} while (song == now_playing);
	
	sound_path[7] = char(song+48);
	now_playing = song;
	return sound_path;
}
void GEngine::place_sprites()
{	
	int x = gmap.getMaxX();
	int y = gmap.getMaxY();
	int rnd_x, rnd_y;	// The labyrithn's edges are walls, so find random x and y inside
	bool f;				// Flag to identify suitability of current position as defined by x and y
	int total_gems = 9 + curr_level;
	int spr_loaded = 0;		// How many sprites have been loaded. Passed as argument at sprite_loaded(int)
	int spr_total = total_gems + 3;	// Total number of sprites to be loaded
	
	ve.init_loading_screen(total_gems);

	for (int i=0; i < total_gems; i++)
	{
		do
		{
			rnd_x = get_random_loc(x-1);
			rnd_y = get_random_loc(y-1);
			f = ((gmap.getType(rnd_x, rnd_y) == PATH));
		} while (!f);
		gmap.pinpoint(rnd_x, rnd_y, GEM);
		ve.sprite_loaded(++spr_loaded, spr_total);
	}

	do
	{
		rnd_x = get_random_loc(x-1);
		rnd_y = get_random_loc(y-1);
		f = ((gmap.getType(rnd_x, rnd_y) == PATH));
	} while (!f);
	gn_mob.setX(rnd_x);
	gn_mob.setY(rnd_y);
	gmap.pinpoint(rnd_x, rnd_y, GNOME);
	ve.sprite_loaded(++spr_loaded, spr_total);
	
	do
	{
		rnd_x = get_random_loc(x-1);
		rnd_y = get_random_loc(y-1);
		f = ((gmap.getType(rnd_x, rnd_y) == PATH));
	} while (!f);
	tr_mob.setX(rnd_x);
	tr_mob.setY(rnd_y);
	gmap.pinpoint(rnd_x, rnd_y, TRAAL);
	ve.sprite_loaded(++spr_loaded, spr_total);
	
	do
	{
		rnd_x = get_random_loc(x-1);
		rnd_y = get_random_loc(y-1);
		f = ((gmap.getType(rnd_x, rnd_y) == PATH));
	} while (!f);
	player.setX(rnd_x);
	player.setY(rnd_y);
	gmap.pinpoint(rnd_x, rnd_y, POTTER);
	ve.sprite_loaded(++spr_loaded, spr_total);
}

void GEngine::createAux(int x, int y)
{
	gmap.createAux(x, y);
}

int GEngine::they_see_me_rollin(int key, bool traal, bool gnome)
{
	int status = 0;
	
	if (traal)
		status = tr_mob.charge();
	if (gnome)
		if (!status)
			status = gn_mob.charge();
	if (key != -1)
		status = player.move(key);
	
	return status;
	
}

void GEngine::pinpoint(int x, int y, int what)
{
	gmap.pinpoint(x, y, what);
}

void GEngine::reset()
{
	score = 0;
	tr_mob.setSpeed(550);
	gn_mob.setSpeed(1100);
	curr_level = 0;
}