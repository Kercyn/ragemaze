#include <cstring>
#include <fstream>
#include <iostream>

#include "hsengine.h"
#include "gengine.h"

extern GEngine ge;

using namespace std;


HSEngine::HSEngine()
{
	char dummy[] = "--------";
	for (int i = 0; i < 5; i++)
	{
		hs[i] << 0;
		hs[i].setName(dummy);
	}
}

void HSEngine::reset()
{
	char dummy[] = "--------";
	for (int i = 0; i < 5; i++)
	{
		hs[i] << 0;
		hs[i].setName(dummy);
	}
}

void HSEngine::load()
{
	ifstream highscores;
	try
	{
		highscores.open("highscores", ios::in | ios::binary);
		int i = 0;
		HS_table temp;
		
		while (highscores.read((char*) &temp, sizeof(temp)))
			hs[i++] = temp;
	}
	catch(exception& e)
	{
		cerr << "HSEngine::load() exception: " << e.what() << endl;
	}
	highscores.close();
}

void HSEngine::add()
{
	ofstream highscores;
	HS_table temp;
	int i, j;
	bool found = false;

	temp.setName(ge.getName());
	temp << ge.getScore();
	try
	{
		for (i = 0; i < 5 && !found; i++)
			if (temp.getScore() > hs[i].getScore())
				found = true;

		if (found)
		{
			for (j = 4; j > i-1; j--)
				hs[j] = hs[j-1];
			hs[j] = temp;
			highscores.open("highscores", ios::out | ios::binary);
			for (i = 0; i < 5; i++)
				highscores.write((char*)&hs[i], sizeof(hs[i]));
			highscores.close();
		}
	}
	catch(exception& e)
	{
		cerr << "HSEngine::add() exception: " << e.what() << endl;
	}
}

HS_table& HSEngine::getRecord(int i)
{
	return hs[i];
}