﻿#ifndef GENGINE_H
#define GENGINE_H

#include "potter.h"
#include "gnome.h"
#include "traal.h"
#include "map.h"

class GEngine
{
	private:
		Map gmap;
		Potter player;
		Gnome gn_mob;
		Traal tr_mob;
		
		int curr_level;
		int max_level;
		int score;
		char *path;
		char *name;
	public:
		void setCurr_level(int);
		void setName(char*);
		void addScore(int);
		int getCurr_level();
		int getMax_level();
		char *getName();
		int getMaxX();
		int getMaxY();
		int getType(int, int);
		int getAux(int, int);
		int getScore();
		int getTrSpeed();
		int getGnSpeed();
		int getPlayerX();
		int getPlayerY();
		bool isPathable(int, int);
		
		char *int_to_str(int);
		void new_level();
		int get_random_loc(int);
		char *get_random_song();
		void place_sprites();
		void createAux(int, int);
		int they_see_me_rollin(int, bool, bool);
		void pinpoint(int, int, int);
		void reset();
		
		GEngine();
		~GEngine();
};
#endif