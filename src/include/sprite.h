﻿#ifndef SPRITE_H
#define SPRITE_H

#include "map.h"

class Sprite : public Tile
{
	protected:
		int x;
		int y;
	public:
		int getX();
		int getY();
		void setX(int);
		void setY(int);
};

#endif