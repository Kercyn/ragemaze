﻿#ifndef VENGINE_H
#define VENGINE_H

#include <ncurses.h>
#include "irrKlang.h"
#pragma comment(lib, "irrKlang.lib")
using namespace irrklang;

class VEngine
{
	private:
		WINDOW *wl;	// New game greetingz
		WINDOW *wm;	// Map
		WINDOW *wh;	// Highscores
	public:
		WINDOW *wd;	// Description
		
		void center(char*, int, WINDOW*, int);
		WINDOW *centerwin(WINDOW*, int, int, int, int);
		bool findCh(char*, char);
		int main_menu();
		void draw_logo();
		void draw_main_menu(int);
		void new_game_menu();
		void new_game_area();
		void init_loading_screen(int);
		void sprite_loaded(int, int);
		int get_key();
		int begin();
		void pin(int, int, int);
		void highscores();
		void credits();
		
		void lulzor();
		void winning_duh();
		
		VEngine();
};

#endif