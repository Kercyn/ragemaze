﻿#ifndef GNOME_H
#define GNOME_H

#include "sprite.h"

class Gnome : public Sprite
{
	private:
		int speed;
		int acc;
	public:
		int charge();
		void accelerate();
		void setSpeed(int);
		int getSpeed();
		Gnome();
};
#endif