﻿#ifndef TILE_H
#define TILE_H

#define PATH			0
#define WALL			1
#define POTTER			2
#define GNOME			4
#define TRAAL			8
#define GEM				16 
#define	SCROLL			32

#define PATH_ICON		" "
#define WALL_ICON		"*"
#define POTTER_ICON		"P"
#define GNOME_ICON		"G"
#define TRAAL_ICON		"T"
#define GEM_ICON		"g"
#define SCROLL_ICON		"s"

class Tile
{
	protected:
		int type;
	public:
		void setType(int);
		int getType();
		bool isPathable();
};

#endif