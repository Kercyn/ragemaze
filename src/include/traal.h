﻿#ifndef TRAAL_H
#define TRAAL_H

#include "sprite.h"

class Traal : public Sprite
{
	private:
		int speed;
		int acc;
	public:
		int charge();
		void accelerate();
		void setSpeed(int);
		int getSpeed();
		Traal();
};
#endif