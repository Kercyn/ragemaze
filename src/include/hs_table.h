#ifndef HS_TABLE_H
#define HS_TABLE_H

class HS_table
{
	private:
		int score;
		char name[11];
	public:
		void setName(char*);
		int getScore();
		char *getName();
		HS_table& operator<<(int);
		HS_table& operator=(HS_table&);
};

#endif