#ifndef HSENGINE_H
#define HSENGINE_H

#include "hs_table.h"

class HSEngine
{
	private:
		HS_table hs[5];
	public:
		void reset();
		void load();
		void add();
		HS_table& getRecord(int);
		
		HSEngine();
};
#endif
