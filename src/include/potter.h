﻿#ifndef POTTER_H
#define POTTER_H

#include "sprite.h"

class Potter : public Sprite
{
	private:
		int gems;
		bool scroll_placed;
	public:
		void reset();
		int move(int);
		Potter();
};
#endif