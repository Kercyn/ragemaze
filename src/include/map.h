﻿#ifndef MAP_H
#define MAP_H

#include <vector>
#include "tile.h"

using namespace std;

class Map
{
	private:
		vector<vector<Tile> > graphic;
		vector<vector<int> > aux;
		int maxX;
		int maxY;
	public:
		int getMaxX();
		int getMaxY();
		int getType(int, int);
		int getAux(int, int);
		bool isPathable(int, int);
		
		void load(char*);
		void pinpoint(int, int, int);
		void createAux(int, int);

		Map();
};

#endif