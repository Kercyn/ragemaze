﻿#include "traal.h"
#include "gengine.h"
#include "vengine.h"

extern GEngine ge;
extern VEngine ve;

int Traal::charge()
{
	ge.createAux(ge.getPlayerX(), ge.getPlayerY());
	ve.pin(x, y, ge.getType(x,y) - TRAAL);
	ge.pinpoint(x, y, ge.getType(x,y) - TRAAL);
	
	if (ge.getAux(x, y-1) == ge.getAux(x,y)-1 && ge.isPathable(x, y-1))
		y--;
	else if (ge.getAux(x, y+1) == ge.getAux(x,y)-1 && ge.isPathable(x, y+1))
		y++;
	else if (ge.getAux(x-1, y) == ge.getAux(x,y)-1 && ge.isPathable(x-1, y))
		x--;
	else if (ge.getAux(x+1, y) == ge.getAux(x,y)-1 && ge.isPathable(x+1, y))
		x++;
	ve.pin(x, y, TRAAL);
	ge.pinpoint(x, y, ge.getType(x,y) + TRAAL);
	
	if (x == ge.getPlayerX() && y == ge.getPlayerY())
		return -1;
	return 0;

}

void Traal::accelerate()
{
	speed -= acc;
}

void Traal::setSpeed(int s)
{
	speed = s;
}

int Traal::getSpeed()
{
	return speed;
}

Traal::Traal()
{
	speed = 550;
	acc = 50;
}