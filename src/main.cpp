#include <ncurses.h>
#include "vengine.h"
#include "gengine.h"
#include "hsengine.h"
#include <iostream>
#include <cstring>

using namespace std;

VEngine ve;
GEngine ge;
HSEngine hse;
ISoundEngine *se = createIrrKlangDevice();
int now_playing = -1;

int main()
{
	int sel, status = 0;
	bool quit = false;
	HS_table temp;

	do
	{
		sel = ve.main_menu();
		switch (sel)
		{
			case 0:
				ve.new_game_menu();
				do
				{
					ge.new_level();
					ge.place_sprites();
					ve.new_game_area();
					status = ve.begin();
				} while (status == 1 && ge.getCurr_level() < ge.getMax_level());
			  
				if (status == -1)
					ve.lulzor();
				else if (status == 1)
					ve.winning_duh();
				else if (status == 2)
				{
					se->removeAllSoundSources();
					se->play2D("sounds/ragequit.ogg");
					napms(3000);
					quit = true;
				}
				hse.add();
				ge.reset();
				break;
			case 1:
				ve.highscores();
				break;
			case 2:
				ve.credits();
				break;
			case 3:
				quit = true;
				se->removeAllSoundSources();
				se->play2D("sounds/epic.ogg");
				napms(3000);
				break;
		}
	} while (!quit);
	se->drop();
	endwin();
	return 0;
}
