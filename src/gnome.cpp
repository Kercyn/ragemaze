#include "gnome.h"
#include "gengine.h"
#include "vengine.h"

extern GEngine ge;
extern VEngine ve;

int Gnome::charge()
{
	ge.createAux(ge.getPlayerX(), ge.getPlayerY());
	ve.pin(x, y, ge.getType(x,y) - GNOME);
	ge.pinpoint(x, y, ge.getType(x,y) - GNOME);
	
	if (ge.getAux(x, y-1) == ge.getAux(x,y)-1 && ge.isPathable(x, y-1))
		y--;
	else if (ge.getAux(x, y+1) == ge.getAux(x,y)-1 && ge.isPathable(x, y+1))
		y++;
	else if (ge.getAux(x-1, y) == ge.getAux(x,y)-1 && ge.isPathable(x-1, y))
		x--;
	else if (ge.getAux(x+1, y) == ge.getAux(x,y)-1 && ge.isPathable(x+1, y))
		x++;
	ve.pin(x, y, GNOME);
	ge.pinpoint(x, y, ge.getType(x,y) + GNOME);
	
	if (x == ge.getPlayerX() && y == ge.getPlayerY())
		return -1;
	return 0;
}

void Gnome::accelerate()
{
	speed -= acc;
}

void Gnome::setSpeed(int s)
{
	speed = s;
}
int Gnome::getSpeed()
{
	return speed;
}

Gnome::Gnome()
{
	speed = 1100;
	acc = 100;
}