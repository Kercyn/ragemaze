﻿#include <ncurses.h>
#include <cstring>
#include <iostream>
#include <fstream>

#include "vengine.h"
#include "gengine.h"
#include "hsengine.h"

using namespace std;

extern GEngine ge;
extern HSEngine hse;
extern ISoundEngine *se;
extern int now_playing;

VEngine::VEngine()
{
	initscr();
	start_color();
	curs_set(0);
	init_pair(1, COLOR_RED, COLOR_BLACK);		// Gnome
	init_pair(2, COLOR_WHITE, COLOR_BLACK);		// Path
	init_pair(3, COLOR_WHITE, COLOR_CYAN);
	init_pair(4, COLOR_GREEN, COLOR_BLACK);		// Traal
	init_pair(5, COLOR_YELLOW, COLOR_BLACK);	// Scroll
	init_pair(6, COLOR_CYAN, COLOR_BLACK);		// Gems
	init_pair(7, COLOR_MAGENTA, COLOR_BLACK);	// Potter
	init_pair(8, COLOR_RED, COLOR_RED);		// Wall
	
	clear();
}

void VEngine::center(char *str, int offsetY = 0, WINDOW* win = stdscr, int offsetX = 0)
{
	int row, col, pos;
	getmaxyx(win, row, col);
	pos = (col - strlen(str))/2;
	mvwprintw(win, offsetY, pos + offsetX, "%s\n", str); 
}

WINDOW *VEngine::centerwin(WINDOW *parent, int lines, int cols, int offsetY = 0, int offsetX = 0)
{
	int startx = (COLS-cols)/2 + offsetX;
	int starty = (LINES-lines)/2 + offsetY;
	return subwin(parent, lines, cols, starty, startx);
}

bool VEngine::findCh(char *src, char what)
{
	int len = strlen(src);
	for (int i = 0; i < len; i++)
		if (src[i] == what)
			return true;
	return false;
}

int VEngine::main_menu()
{
	int item = 0, key;
	
	clear();
	noecho();
	flushinp();
	nodelay(stdscr, false);
	keypad(stdscr, true);
	se->removeAllSoundSources();
	se->play2D(ge.get_random_song());
	draw_logo();
	draw_main_menu(item);

	try
	{
		do
		{
			key = getch();
			switch (key)
			{
				case KEY_DOWN:
					item++;
					if (item > 3)
						item = 0;
					break;
				case KEY_UP:
					item--;
					if (item < 0)
						item = 3;
					break;
				default:
					break;
			}
			draw_main_menu(item);
		} while(key != '\n' && key != 27);
		
		if (key == 27)
		      throw key;
	}
	catch(int whichKey)
	{
		if (key == 27)
			return 3;
	}
	return item;
}

void VEngine::draw_logo()
{
	char title[][50] = {
	  " _____                  __  __               _ _ ",
	  "|  __ \\                |  \\/  |             | | |",
	  "| |__) |__ _  __ _  ___| \\  / | __ _ _______| | |",
	  "|  _  // _` |/ _` |/ _ \\ |\\/| |/ _` |_  / _ \\ | |",
	  "| | \\ \\ (_| | (_| |  __/ |  | | (_| |/ /  __/_|_|",
	  "|_|  \\_\\__,_|\\__, |\\___|_|  |_|\\__,_/___\\___(_|_)",
	  "              __/ |                              ",
	  "             |___/                               "
	};
	clear();

	attrset(COLOR_PAIR(1));	// Red foreground, Black background
	for (int i = 0; i < 8; i++)
		center(title[i], 1+i);
}


void VEngine::draw_main_menu(int item)
{
	char menu_items[][11] = {
		"New Game",
		"Highscores",
		"Credits",
		"Quit"
		};
		
	clrtobot();

	attrset(COLOR_PAIR(2));	// White foreground, Black background
	for (int i = 0; i < 4; i++)
	{
		if (i == item)
		    attron(A_REVERSE);
		center(menu_items[i], 11+i);
		attroff(A_REVERSE);
	}
	
}

void VEngine::new_game_menu()
{
	int row, col, pos;
	char title[] = "* * NEW GAME * *";
	char *centmsg, name[11],  msg1[] = "Welcome ",
				  msg2[] = "Press any key to",
				  msg3[] = "START THE RAEG!!1",
				  ctrl1[] = "Controls",
				  ctrl2[] = "Move with the arrow keys, pause the game with Space",
				  name1[] = "Your name: ",
				  name2[] = "(1-10 characters, no whitespace)";
	WINDOW *msgwin;
	
	echo();
	clear();
	flushinp();
	
	attrset(COLOR_PAIR(2));	// White foreground, Black background
	
	attron(A_BOLD | A_UNDERLINE);
	center(title, 2);
	attroff(A_BOLD | A_UNDERLINE);
	
	center(name2, 6, stdscr, -10);
	attron(A_BOLD | A_UNDERLINE);
	center(ctrl1, 20);
	attroff(A_BOLD | A_UNDERLINE);
	center(ctrl2, 21);
	
	getmaxyx(stdscr, row, col);
	pos = col/2;
	
	do
	{
	  center(name1, 5, stdscr, -16);
	  mvgetnstr(5, pos - 10, name, 10);
	} while (strlen(name) <= 0 || findCh(name, ' ') == true || findCh(name, '\t'));

	msgwin = centerwin(stdscr, 7, 36);
	
	if (msgwin == NULL)
	{
	  strcat(msg1, name);
	  strcat(msg1, "!");
	  center(msg1, 10);
	  center(msg2, 11);
	  center(msg3, 12);
	}
	else
	{
	  wbkgd(msgwin, COLOR_PAIR(3));	// White foreground, Cyan background

	  centmsg = new char[strlen(msg1) + strlen(name) + 1];
	  strcpy(centmsg, msg1);
	  strcat(centmsg, name);
	  strcat(centmsg, "!");
	  
	  center(centmsg, 2, msgwin);
	  center(msg2, 3, msgwin);
	  center(msg3, 4, msgwin);
	  
	  wborder(msgwin, 0, 0, 0, 0, 0, 0, 0, 0);
	  wrefresh(msgwin);
	  
	}
	ge.setName(name);

	getch();
	delete[] centmsg;
	delwin(msgwin);
}

void VEngine::new_game_area()
{
	char title[] = "* * RageMaze!! * *",
	     subtitle[] = "Press any key to begin...",
	     level[] = "Level x",
	     score[] = "Score:      ",
	     gems[] = "Gems : 00/xx",
	     speed[] = "Monsters' speed",
	     traal[] = "Traal:  xxx",
	     gnome[] = "Gnome:  xxx",
	     hs[] = "Highscores",
	     
	     num_level[] = "x",
	     num_score[] = "xxxx",
	     num_gems[] = "xx",
	     num_curr_gems[] = " 0/",
	     num_traal[] = "xxxx",
	     num_gnome[] = "xxx",
	     name[11];
	     
	     strcpy(num_level, ge.int_to_str(ge.getCurr_level()));
	     if (ge.getCurr_level() == 1)
			strcpy(num_score, "   0");
	     else
			strcpy(num_score, ge.int_to_str(ge.getScore()));
	     strcpy(num_gems, ge.int_to_str(9 + ge.getCurr_level()));
	     strcpy(num_traal, ge.int_to_str(ge.getTrSpeed()));
	     strcpy(num_gnome, ge.int_to_str(ge.getGnSpeed()));	     
	     
	int lev = ge.getCurr_level();
	int wmpos = -((ge.getMaxX()+2)/2);
	int wdpos = (ge.getMaxX()+2)/2;
	int max_x = ge.getMaxX();
	int max_y = ge.getMaxY();
	
	HS_table temp;
	char temp_name[11];
	int temp_score;

	strcpy(name, ge.getName());
	se->play2D("sounds/fqaq.ogg");
	
	clear();
	noecho();
	
	attron(COLOR_PAIR(1) | A_BOLD);		// Red foreground, black background
	center(title, 2);
	attroff(COLOR_PAIR(1) | A_BOLD);
	center(subtitle, 4);
	
	wm = centerwin(stdscr, ge.getMaxY()+2, ge.getMaxX()+2, -1, wmpos);
	wd = centerwin(stdscr, 11, ge.getMaxY()+2, -6, wdpos);
	wh = centerwin(stdscr, 11, ge.getMaxY()+2, 5, wdpos);	
	
	wbkgd(wm, COLOR_PAIR(2));
	wbkgd(wd, COLOR_PAIR(2));
	wbkgd(wh, COLOR_PAIR(2));
	
	  ///////////////////////////////////////////////////////////
	 ////			Description			////
	///////////////////////////////////////////////////////////
	wattron(wd, COLOR_PAIR(4) | A_BOLD);	// Green foreground, black background
	center(level, 1, wd);
	center(num_level, 1, wd, 3);
	wattroff(wd, COLOR_PAIR(4) | A_BOLD);
	
	wattron(wd, COLOR_PAIR(2) | A_BOLD);	// White foreground, black background
	center(name, 2, wd);
	wattroff(wd, COLOR_PAIR(2) | A_BOLD);
	
	wattron(wd, COLOR_PAIR(5) | A_BOLD);	// Yellow foreground, black background
	center(score, 4, wd);
	wattroff(wd, COLOR_PAIR(5) | A_BOLD);
	wattron(wd, COLOR_PAIR(2));
	center(num_score, 4, wd, 3);
	wattroff(wd, COLOR_PAIR(2));
	
	wattron(wd, COLOR_PAIR(6) | A_BOLD);	// Cyan foreground, black background
	center(gems, 5, wd);
	wattroff(wd, COLOR_PAIR(6) | A_BOLD);
	wattron(wd, COLOR_PAIR(2));
	center(num_curr_gems, 5, wd, 2);
	center(num_gems, 5, wd, 4);
	wattroff(wd, COLOR_PAIR(2));
	
	wattron(wd, COLOR_PAIR(1));		// Red foreground, black background
	center(speed, 7, wd);
	wattroff(wd, COLOR_PAIR(1));
	
	wattron(wd, COLOR_PAIR(4) | A_BOLD);
	center(traal, 8, wd);
	wattroff(wd, COLOR_PAIR(4) | A_BOLD);
	wattron(wd, COLOR_PAIR(2));
	center(num_traal, 8, wd, 4);
	wattroff(wd, COLOR_PAIR(2));
	
	wattron(wd, COLOR_PAIR(1) | A_BOLD);
	center(gnome, 9, wd);
	wattroff(wd, COLOR_PAIR(1) | A_BOLD);
	wattron(wd, COLOR_PAIR(2));
	(ge.getCurr_level() == 1) ? center(num_gnome, 9, wd, 3) : center(num_gnome, 9, wd, 4);
	wattroff(wd, COLOR_PAIR(2));
	
	  ///////////////////////////////////////////////////////////
	 ////				Map			////
	///////////////////////////////////////////////////////////
	for (int i = 1; i < max_x + 1; i++)
		for (int j = 1; j < max_y + 1; j++)
			switch (ge.getType(i-1, j-1))
			{
				 case PATH:
					wattron(wm, COLOR_PAIR(2));
					mvwprintw(wm, j, i, PATH_ICON);
					wattroff(wm, COLOR_PAIR(2));
					break;
				 case WALL:
					wattron(wm, COLOR_PAIR(8));
					mvwprintw(wm, j, i, WALL_ICON);
					wattroff(wm, COLOR_PAIR(8));
					break;
				 case POTTER:
					wattron(wm, COLOR_PAIR(7) | A_BOLD);
					mvwprintw(wm, j, i, POTTER_ICON);
					wattroff(wm, COLOR_PAIR(7) | A_BOLD);
					break;
				 case TRAAL:
					wattron(wm, COLOR_PAIR(4) | A_BOLD);
					mvwprintw(wm, j, i, TRAAL_ICON);
					wattroff(wm, COLOR_PAIR(4) | A_BOLD);
					break;
				 case GNOME:
					wattron(wm, COLOR_PAIR(1) | A_BOLD);
					mvwprintw(wm, j, i, GNOME_ICON);
					wattroff(wm, COLOR_PAIR(1) | A_BOLD);
					break;
				 case GEM:
					wattron(wm, COLOR_PAIR(6));
					mvwprintw(wm, j, i, GEM_ICON);
					wattroff(wm, COLOR_PAIR(6));
					break;
			}
	
	  ///////////////////////////////////////////////////////////
	 ////			Highscores			////
	///////////////////////////////////////////////////////////
	wattron(wh, COLOR_PAIR(4) | A_BOLD);
	center(hs, 1, wh);
	wattroff(wh, COLOR_PAIR(4) | A_BOLD);
	
	hse.load();
	for (int i = 0; i < 5; i++)
	{
		temp = hse.getRecord(i);
		strcpy(temp_name, temp.getName());
		temp_score = temp.getScore();
		mvwprintw(wh, i+3, 4, temp_name);
		mvwprintw(wh, i+3, 16, ge.int_to_str(temp_score));
	}
	
	wborder(wm, 0, 0, 0, 0, 0, 0, 0, 0);
	wborder(wd, 0, 0, 0, 0, 0, 0, 0, 0);
	wborder(wh, 0, 0, 0, 0, 0, 0, 0, 0);
	wrefresh(wm);
	wrefresh(wd);
	wrefresh(wh);
	flushinp();
	nodelay(stdscr, false);
	getch();
	
}

void VEngine::init_loading_screen(int gems)
{
	char loading[] = "Loading...",
	     edge1[] = "[",
	     edge2[] = "]";
	int arity = gems + 3;		// The arity of sprites that need to be loaded, so that the length and position of the loading bar can be determined

	clear();
	int startx = (COLS - 35)/2;
	int starty = (LINES - 7)/2;
	wl = subwin(stdscr, 7, 35, starty, startx);
	se->removeAllSoundSources();
	
	if (wl == NULL)
	{
		center(loading, 5);
	}
	else
	{
		wbkgd(wl, COLOR_PAIR(2)); // White foreground, black background

		attron(A_BOLD);
		center(loading, 2, wl);
		attroff(A_BOLD);
		
		center(edge1, 5, wl, -(arity/2) - 1);
		center(edge2, 5, wl, arity/2);
		wrefresh(wl);
	}
	refresh();
}

void VEngine::sprite_loaded(int s, int a)
{
	char bar[] = "=",
	     edge2[] = "]";
	int arity = a;
	center(bar, 5, wl, -arity/2 - 1 + s);
	center(edge2, 5, wl, arity/2);
	wrefresh(wl);
}

int VEngine::get_key()
{
	int ch = getch();
	
	if (ch != ERR)
	{
		ungetch(ch);
		return 1;
	}
	return 0;
}

int VEngine::begin()
{
	int key, tr_counter = 0, gn_counter = 0, i, status = 0;
	bool move_traal, move_gnome;
	char paused_msg[] = "Game paused. Press any key to resume...";
	
	keypad(stdscr, true);
	nodelay(stdscr, true);
	noecho();
	flushinp();
	move (4,0);
	cbreak();
	clrtoeol();
	se->removeAllSoundSources();
	se->play2D(ge.get_random_song());
	
	try
	{
		do
		{
			move_traal = move_gnome = false;
			
			if (get_key())
				key = getch();
			else
				key = -1;

			if (tr_counter == ge.getTrSpeed())
			{
				tr_counter = 0;
				move_traal = true;
			}
			if (gn_counter == ge.getGnSpeed())
			{
				gn_counter = 0;
				move_gnome = true;
			}

			if (key == 32)
			{
				center(paused_msg, 4);
				nodelay(stdscr, false);
				getch();
				move(4,0);
				clrtoeol();
				nodelay(stdscr, true);
			}
			
			if (key == 27)
				throw(27);
			
			status = ge.they_see_me_rollin(key, move_traal, move_gnome);
			
			flushinp();
			napms(50);
			tr_counter += 50;
			gn_counter += 50;
		} while (status == 0);
	}
	catch (int whichKey)
	{
		return 2;
	}
	
	delwin(wm);
	delwin(wd);
	delwin(wh);
	
	return status;
}

void VEngine::pin(int x, int y, int what)
{
	switch (what)
	{
		case PATH:
			mvwprintw(wm, y+1, x+1, PATH_ICON);
			break;
		case WALL:
			wattron(wm, COLOR_PAIR(2));
			mvwprintw(wm, y+1, x+1, WALL_ICON);
			wattroff(wm, COLOR_PAIR(2));
			break;
		  case POTTER:
			wattron(wm, COLOR_PAIR(7) | A_BOLD);
			mvwprintw(wm, y+1, x+1, POTTER_ICON);
			wattroff(wm, COLOR_PAIR(7) | A_BOLD);
			break;
		  case TRAAL:
			wattron(wm, COLOR_PAIR(4) | A_BOLD);
			mvwprintw(wm, y+1, x+1, TRAAL_ICON);
			wattroff(wm, COLOR_PAIR(4) | A_BOLD);
			break;
		  case GNOME:
			wattron(wm, COLOR_PAIR(1) | A_BOLD);
			mvwprintw(wm, y+1, x+1, GNOME_ICON);
			wattroff(wm, COLOR_PAIR(1) | A_BOLD);
			break;
		  case GEM:
			wattron(wm, COLOR_PAIR(6));
			mvwprintw(wm, y+1, x+1, GEM_ICON);
			wattroff(wm, COLOR_PAIR(6));
			break;
		  case SCROLL:
			 wattron(wm, COLOR_PAIR(5) | A_BOLD);
			 mvwprintw(wm, y+1, x+1, SCROLL_ICON);
			 wattroff(wm, COLOR_PAIR(5));
			 break;
		}
	wrefresh(wm);
}


void VEngine::highscores()
{
	char title[] = "Highscores",
	     bottom1[] = "q: Back to Main Menu",
	     bottom2[] = "r: Reset highscores",
	     conf1[] = "Are you sure you want",
	     conf2[] = "to reset your highscores?",
	     conf3[] = "(y/n)",
	     resp,
	     key;
	int i;
	HS_table temp;
	WINDOW *wc;
	
	clear();
	noecho();
	flushinp();
	
	attron(COLOR_PAIR(4) | A_BOLD);
	center(title, 3);
	attroff(COLOR_PAIR(4) | A_BOLD);
	
	hse.load();
	for (i = 0; i < 5; i++)
	{
		temp = hse.getRecord(i);
		center(temp.getName(), i+5, stdscr, -4);
		center(ge.int_to_str(temp.getScore()), i+5, stdscr, 8);
	}
	
	center(bottom1, i+8);
	center(bottom2, i+9);
	do
	{
		key = getch();
	} while(key != 'q' && key != 'Q' && key != 'r' && key != 'R');
	
	if (key == 'r' || key == 'R')
	{
		wc = centerwin(stdscr, 7, 35);
		wbkgd(wc, COLOR_PAIR(3));	// White foreground, Cyan background

		center(conf1, 2, wc);
		center(conf2, 3, wc);
		center(conf3, 4, wc);
		
		wborder(wc, 0, 0, 0, 0, 0, 0, 0, 0);
		wrefresh(wc);
		resp = wgetch(wc);
		delwin(wc);
	}
	
	if (resp == 'y' || resp == 'Y')
	{
		try
		{
			fstream hs;
			hs.open("highscores", ios::out | ios::trunc);
			hs.close();
			hse.reset();
		}
		catch(exception& e)
		{
			cerr << "VEngine::highscores() exception: " << e.what() << endl;
		}
	}
}

void VEngine::credits()
{
	char sound_title[] = "** Sounds **",
	     ascii_title[] = "** ASCII Art **",
	     irrKlang_lib[] = "RageMaze!! uses the irrKlang library.",
	     sounds[][71] = {
	       "          0.ogg: Eluveitie - Inis Mona (8bit)                        ",
	       "          1.ogg: Eluveitie - Thousandfold (8bit)                     ",
	       "          2.ogg: Pirates of the Carribean - He's a Pirate! (8bit)    ",
	       "       epic.ogg: Clip taken from Harold and Kumar - The White Castle ",
	       "       fqaq.ogg,                                                     ",
	       "   ragequit.ogg,                                                     ",
	       "  the_king!.ogg: Taken from Heroes of Newerth, a game by S2 Games    ",
	       "trololololo.ogg: Eduard Khil - I Am Glad, 'Cause I'm Finally Returning Back Home"},
	     ascii[][57] = {
	       "RageMaze!! title, converted from text to ASCII art using",
	       "http://www.network-science.de/ascii/",
	       "Reapers and Crown from",
	       "http://www.geocities.com/spunk1111"},
	     exit[] = "Press 'q' to go back.",
	     hurr[] = "Developed by Spyros Laskaris-Trigkas.",
	     key;
	       
	int i;
	
	clear();
	noecho();
	flushinp();
	
	attron(COLOR_PAIR(1) | A_BOLD);
	center(sound_title, 3);
	attroff(COLOR_PAIR(1) | A_BOLD);
	attron(COLOR_PAIR(2));
	for (i = 0; i < 7; i++)
		center(sounds[i], i+5);
	attroff(COLOR_PAIR(2));
	
	attron(COLOR_PAIR(6) | A_BOLD);
	center(ascii_title, 13);
	attroff(COLOR_PAIR(6) | A_BOLD);
	attron(COLOR_PAIR(2));
	for (i = 0; i < 4; i++)
		center(ascii[i], i+15);
	attron(A_BOLD);
	center(hurr, 21);
	attroff(A_BOLD);
	attroff(COLOR_PAIR(2));
	
	attron(COLOR_PAIR(5) | A_BOLD);
	center(irrKlang_lib, 22);
	attroff(COLOR_PAIR(5));
	attron(COLOR_PAIR(4));
	center(exit, 23);
	attroff(COLOR_PAIR(4) | A_BOLD);
	
	do
	{
		key = getch();
	} while (key != 'q' && key != 'Q');     
}

void VEngine::lulzor()
{
	int i;
	char key;
	char msg[] = "Press 'q' to quit.";
	char qq[][91] = {
"                      ,____                                           ____,               ",
"                      |---.\\                                         /.---|               ",
"              ___     |    `                                         `    |     ___       ",
"             / .-\\  ./=)                                                 (=\\.  /-. \\      ",
"            |  |\"|_/\\/|                                                   |\\/\\_|\"|  |     ",
"            ;  |-;| /_|               EPIC LOSER KTHNXBAI!!               |_\\ |;-|  ;     ",
"           / \\_| |/ \\ |                                                   | / \\| |_/ \\    ",
"          /      \\/\\( |                                                   | )/\\/      \\   ",
"          |   /  |` ) |                                                   | ( '|  \\   |   ",
"          /   \\ _/    |                                                   |    \\_ /   \\   ",
"         /--._/  \\    |                                                   |    /  \\_.--\\  ",
"         `/|)    |    /                                                   \\    |    (|\\`  ",
"           /     |   |                   Come again!                       |   |     \\    ",
"         .'      |   |                                                     |   |      '.  ",
"        /         \\  |                                                     |  /         \\ ",
"       (_.-.__.__./  /                                                     \\  \\.__.__.-._)"};      
	  
	se->removeAllSoundSources();
	se->play2D("sounds/trololololo.ogg");
	clear();
	noecho();
	nodelay(stdscr, false);
	flushinp();
	for (i = 0; i < 16; i++)
		center(qq[i], 6+i);
	center(msg, i + 8);
	
	do
	{
		key = getch();
	} while (key != 'q' && key != 'Q');
}

void VEngine::winning_duh()
{
	int i;
	char key;
	char msg[] = "Press 'q' to quit.";
	char crown[][34] = {
"                _                ",
"              _\\ /_              ",
"              >_X_<              ",
"       .---._  /_\\  _.---.       ",
"     /`.---._`{/ \\}`_.---.`\\     ",
"    | /   ___`{\\_/}`___   \\ |    ",
"    \\ \\.\"`*  `\"{_}\"`  *`\"./ /    ",
"     \\ \\  )\\  _\\ /_  /(  / /     ",
"      \\ *<()( >_X_< )()>* /      ",
"       |._)/._./_\\._.\\(_.|       ",
"       |() () () () () ()|       ",
"       <<o>><<o>><o>><<o>>       ",
"      `\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"`      "};

	se->removeAllSoundSources();
	se->play2D("sounds/the_king!.ogg");
	clear();
	noecho();
	nodelay(stdscr, false);
	flushinp();

	attron(COLOR_PAIR(6));
	for (i = 0; i < 13; i++)
		center(crown[i], i + 6);
	attroff(COLOR_PAIR(6));
	
	center(msg, i + 8);
	do
	{
		key = getch();
	} while (key != 'q' && key != 'Q');
}