﻿#include "tile.h"

void Tile::setType(int t)
{
	type = t;
}

int Tile::getType()
{
	return type;
}

bool Tile::isPathable()
{
	switch (type)
	{
		case WALL:
		case GNOME:
		case TRAAL:
		case (GNOME | GEM):
		case (GNOME | SCROLL):
		case (TRAAL | GEM):
		case (TRAAL | SCROLL):
			return false;
		case PATH:
		case POTTER:
		case GEM:
		case SCROLL:
			return true;
	}
}