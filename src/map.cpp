﻿#include <fstream>
#include <iostream>
#include <cstring>
#include <ctime>
#include <cstdlib>

#include "map.h"
#include "tile.h"
#include "sprite.h"

using namespace std;

Map::Map()
{
	graphic.resize(1);
}
int Map::getMaxX()
{
	return maxX;
}

int Map::getMaxY()
{
	return maxY;
}

int Map::getType(int x, int y)
{
	return graphic[y][x].getType();
}

int Map::getAux(int x, int y)
{
	return aux[y][x];
}

bool Map::isPathable(int x, int y)
{
	return graphic[y][x].isPathable();
}

void Map::load(char *path)
{
	ifstream level;
	char *line;
	int height = 0, max = -1, len;
	line = new char[100];
	
	try
	{
		level.open(path);
		
		while (level.getline(line, 100), level.good())
		{
			
			len = strlen(line);
			if (len > max)
				max = len;
			graphic.resize(height+1);
			aux.resize(height+1);
			graphic[height].resize(len);
			aux[height].resize(len);
			for (int i = 0; i < len; i++)
				(line[i] == WALL_ICON[0]) ? graphic[height][i].setType(WALL) : graphic[height][i].setType(PATH);
			height++;
		}
	}
	
	catch (exception& e)
	{
		cerr <<	"Map::load exception: " << e.what() << endl;
	}
	
	delete[] line;
	level.close();
	maxX = max;
	maxY = height;
}

void Map::pinpoint (int x, int y, int type)
{
	graphic[y][x].setType(type);
}

void Map::createAux(int x, int y)
{
	int i, j;
	bool f;
	
	for (i = 0; i < maxX; i++)
		for (j = 0; j < maxY; j++)
			aux[j][i] = (graphic[j][i].getType() == WALL) ? -1 : -2;
	aux[y][x] = 0;
	
	do
	{
		f = false;
		for (i = 1; i < maxX-1; i++)
			for (j = 1; j < maxY-1; j++)
			{
				if (aux[j][i] == -2 && aux[j][i-1] >= 0)
				{
					f = true;
					aux[j][i] = aux[j][i-1] + 1;
				}
				if (aux[j][i] == -2 && aux[j][i+1] >= 0)
				{
					f = true;
					aux[j][i] = aux[j][i+1] + 1;
				}
				if (aux[j][i] == -2 && aux[j-1][i] >= 0)
				{
					f = true;
					aux[j][i] = aux[j-1][i] + 1;
				}
				if (aux[j][i] == -2 && aux[j+1][i] >= 0)
				{
					f = true;
					aux[j][i] = aux[j+1][i] + 1;
				}
			}
	} while (f);
}